{% for user in salt['pillar.get']('admin:users', ['root']) %}
{{ user }}_bashrc:
    file.managed:
    {% if user == 'root' %}
        - name: /root/.bashrc
    {% else %}
        - name: /home/{{ user }}/.bashrc
    {% endif %}
        - mode: 644
        - user: {{ user }}
        - source: salt://admin/files/bashrc.jinja
        - template: jinja
        - context:
            user: {{ user }}
{% endfor %}
