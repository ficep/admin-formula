{% for user in salt['pillar.get']('admin:users', ['root']) %}
{{ user }}_vimrc:
    file.managed:
    {% if user == 'root' %}
        - name: /root/.vimrc
    {% else %}
        - name: /home/{{ user }}/.vimrc
    {% endif %}
        - mode: 644
        - user: {{ user }}
        - source: salt://admin/files/vimrc
{% endfor %}
