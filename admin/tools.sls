{% from 'admin/map.jinja' import admin with context %}

{% for tool in admin.tools %}
install_{{ tool }}:
    pkg.installed:
        - name: {{ tool }}
{% endfor %}
