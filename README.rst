=====
admin
=====

Formula to manage users' configuration.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.


Available states
================

.. contents::
    :local:

``admin.bashrc``
----------------

Set the ``.bashrc`` file.

``admin.tools``
---------------

Install various administration tools. The default list includes ``colordiff``,
``curl``, ``htop``, ``multitail``, ``wget``. The tools list can be changed
with pillar as follows

.. code:: yaml

    admin:
        tools:
            - unzip
            - git
            - subversion

``admin.vimrc``
---------------

Set the ``.vimrc`` file.

References
==========

-  `Salt Formulas <https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`__
